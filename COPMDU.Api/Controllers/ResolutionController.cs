﻿using System;
using System.Collections.Generic;
using COPMDU.Domain;
using COPMDU.Infrastructure.Interface;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace COPMDU.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ResolutionController : ControllerBase
    {
        private readonly IResolutionRepository _resolutionRepository;
        private readonly ILogRepository _logRepository;

        public ResolutionController(IResolutionRepository resolutionRepository, ILogRepository logRepository)
        {
            _resolutionRepository = resolutionRepository;
            _logRepository = logRepository;
        }

        [HttpGet]
        public IEnumerable<Resolution> Get()
        {
            try
            {
                return _resolutionRepository.Get();
            }
            catch (Exception ex)
            {
                _logRepository.RecordException(ex);
                throw;
            }
        }

        [HttpGet("{id}")]
        public Resolution Get(int id)
        {
            try
            {
                return _resolutionRepository.GetById(id);
            }
            catch (Exception ex)
            {
                _logRepository.RecordException(ex);
                throw;
            }
        }
    }
}